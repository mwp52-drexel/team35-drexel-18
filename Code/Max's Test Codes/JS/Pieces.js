function multiMove(dir,myPiece){ //dir: format 0/1 for x/y ex. [0,1]
    var rList = [];
    var next = true;
    var vert = 0;
    var horiz = 0;
    for (var z = 0; z < dir.length; z++){
        console.log("Direction way = ",dir[z]); //FIXME
        next = true;
        while(next == true){ //up
            vert = vert + dir[z][1];
            var y = j + vert;
            horiz = horiz + dir[z][0];
            var x = i + horiz;
            // out of board
            if(y < 0 || y > 7 || x < 0 || x >7){next = false;}
            // empty space
            else if(pieces[y][x] == 0){rList.push([horiz,vert]);}
            // black piece to white piece
            else if(pieces[y][x] > 0 && myPiece < 0){rList.push([horiz,vert]); next = false;}
            // white piece to black piece
            else if(pieces[y][x] < 0 && myPiece > 0){rList.push([horiz,vert]); next = false;}
            // same type piece
            else{break;}
        }
        vert = 0;
        horiz = 0;
    }
    console.log("Rook list = ",rList);
    return rList;
}

function pieceMoves(myPiece){
	var rList = [];
	if (myPiece == -1) { //black pawn
			console.log("tdID = ", tdID);
			console.log("I and J =", i, j);
			if (j+1 > 7){return rList;}
			if (pieces[j+1][i+0] == 0){
					rList.push([0,+1]);
					if (tdID > 7 && tdID <16 && pieces[j+2][i+0] == 0){rList.push([0,+2]);}
			}
			if (pieces[j+1][i+1] > 0){rList.push([+1,+1]);}
			if (pieces[j+1][i-1] > 0){rList.push([-1,+1]);}
			return rList;
	}
	else if (myPiece == 1) { //white pawn
			console.log("tdID = ", tdID);
			console.log("I and J =", i, j);
			if (j-1 < 0){return rList;}
			if (pieces[j-1][i+0] == 0){
					rList.push([0,-1]);
					if (tdID > 47 && tdID <56 && pieces[j-2][i+0] == 0){rList.push([0,-2]);}
			}
			if (pieces[j-1][i-1] < 0){rList.push([-1,-1]);}
			if (pieces[j-1][i+1] < 0){rList.push([+1,-1]);}
			return rList;
	}
	else if (myPiece == 2 || myPiece == -2) { //rooks
			var dir = [
			    [0,-1], //up
                [0,1],  //down
                [1,0],  //right
                [-1,0], //left
            ];
			return multiMove(dir,myPiece);
	}
	else if (myPiece == 3 || myPiece == -3) { //knights
		var kmoves = [
		    [-1,-2],
            [-2,-1],
            [1, -2],
            [2, -1],
            [-1,2],
            [-2,1],
            [1,2],
            [2,1]
        ];
		for (var move in kmoves) {
            var x = i + kmoves[move][0];
            var y = j + kmoves[move][1];
            if ((x < 8 && x >= 0) && (y < 8 && y >= 0)) {
                if (myPiece == 3 && pieces[y][x] <= 0){
                    rList.push(kmoves[move]);
                }
                else if (myPiece == -3 && pieces[y][x] >= 0){
                    rList.push(kmoves[move]);
                }
            }
        }
        return rList;
	}
	else if (myPiece == 4 || myPiece == -4){ //bishops
            var dir = [
                [1,-1], //up,right
                [-1,-1],  //up, left
                [1,1],  //down, right
                [-1,1], //down, left
            ];
            return multiMove(dir,myPiece);
	}
    else if (myPiece == 5 || myPiece == -5){ //queens
        var dir = [
            [0,-1], //up
            [0,1],  //down
            [1,0],  //right
            [-1,0], //left
            [1,-1], //up,right
            [-1,-1],  //up, left
            [1,1],  //down, right
            [-1,1], //down, left
        ];
        return multiMove(dir,myPiece);
    }
	else if (myPiece == 6 || myPiece == -6){ //kings
		var kingdir = [
		    [1,0],
            [-1,0],
            [0,1],
            [0,-1],
            [1,1],
            [1,-1],
            [-1,1],
            [-1,-1]
        ];

		for (var move = 0; move<kingdir.length;move++){
            var x = kingdir[move][0] + i;
            var y = kingdir[move][1] + j;
            var ea = [];
            // out of board
            if(y < 0 || y > 7 || x < 0 || x >7 || pieces[y][x]*turn >= 1){continue;}
		    else if (threatened([x,y],turn*-1) == false){
		        rList.push(kingdir[move]);
            }
        }
		return rList;
	}

}