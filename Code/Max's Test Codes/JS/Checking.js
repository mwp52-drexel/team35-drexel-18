function threatened(pos,colorcheck){ // pos: [x,y]
    var threatspaces = [];
    var bishopdir = [
        [1,-1], //up,right
        [-1,-1],  //up, left
        [1,1],  //down, right
        [-1,1], //down, left
    ];
    var rookdir = [
        [0,-1], //up
        [0,1],  //down
        [1,0],  //right
        [-1,0], //left
    ];
    var knightdir = [
        [-1,-2],
        [-2,-1],
        [1, -2],
        [2, -1],
        [-1,2],
        [-2,1],
        [1,2],
        [2,1]
    ];

    //BISHOP/QUEEN
    for (var z = 0; z < bishopdir.length; z++) {
        var xstep = bishopdir[z][0];
        var ystep = bishopdir[z][1];
        var x = pos[0];
        var y = pos[1];
        var step = 0;

        for (step; step >= 0; step++){
            x += xstep;
            y += ystep;

            if(y < 0 || y > 7 || x < 0 || x >7){step = -2;continue;}

            if (step==0 && ((pieces[y][x] == 1*colorcheck && ystep*colorcheck > 0) || pieces[y][x] == 6*colorcheck)){   //PAWNS/KING
                redhighlight(getID(x,y));
                threatspaces.push([x,y]);
            }

            if (pieces[y][x] == 4*colorcheck || pieces[y][x] == 5*colorcheck){ //BISHOP(4) QUEEN(5)
                redhighlight(getID(x,y));
                threatspaces.push([x,y]);
            }

            if (pieces[y][x] != 0 && pieces[y][x] != 6*colorcheck*-1){step = -2;}
        }
    }

    //ROOK/QUEEN
    for (var z = 0; z < rookdir.length; z++) {
        var xstep = rookdir[z][0];
        var ystep = rookdir[z][1];
        var x = pos[0];
        var y = pos[1];
        var step = 0;

        for (step; step >= 0; step++){
            x += xstep;
            y += ystep;

            if(y < 0 || y > 7 || x < 0 || x >7){step = -2;continue;}

            if (step==0 && pieces[y][x] == 6*colorcheck){ //KING
                redhighlight(getID(x,y));
                threatspaces.push([x,y]);
            }

            if (pieces[y][x] == 2*colorcheck || pieces[y][x] == 5*colorcheck){ //ROOK(2) QUEEN(5)
                redhighlight(getID(x,y));
                threatspaces.push([x,y]);
            }

            if (pieces[y][x] != 0 && pieces[y][x] != 6*colorcheck*-1){step = -2;}
        }
    }

    //KNIGHTS
    for (var z = 0; z < knightdir.length; z++){
        var x = pos[0] + knightdir[z][0];
        var y = pos[1] + knightdir[z][1];

        if(y < 0 || y > 7 || x < 0 || x >7){next = false;}

        else if (pieces[y][x] == 3*colorcheck || pieces[y][x] == 3*colorcheck){ //KNIGHT(3)
            redhighlight(getID(x,y));
            threatspaces.push([x,y]);
        }
    }
    console.log("Threat on",pos,"from",threatspaces);
    if (threatspaces.length == 0){return false;}
    else {return threatspaces;}
}

