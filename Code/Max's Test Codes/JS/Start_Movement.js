turn = 1;
tdID = null;

function start(cellID){
    unhighlight();
    //console.log("CellID = ", cellID); //FIXME
    tdID = cellID;
    //console.log("turn = ", turn); //FIXME
    [i,j] = getCoords(cellID); // i,j is the source of the piece move
    console.log("Clicked cell Coords = ",i,j); //FIXME
    var mypiece = pieces[j][i];
    //console.log('Piece type =', mypiece); //FIXME
    if (mypiece != 0 && turn*mypiece > 0) {
        var myMoves = pieceMoves(mypiece);
        console.log('Possible Moves (2D) Array = ', myMoves); //FIXME
        for (var move in myMoves){
            var [x,y] = myMoves[move];
            //console.log("Relative Move Coordinate =",x,y);  //FIXME
            x = parseInt(x) + parseInt(i);
            y = parseInt(y) + parseInt(j);
            if (legalMoves(i,j,x,y)){    //legalMoves removes possible moves based on edges of board, your own pieces, being in check
                //console.log("Real Move Coordinate =",x,y);  //FIXME
                var legalSq = getID(x,y);
                highlight(legalSq);
            }
        }
    }
}

function moveShit(cellID){
    var [x,y] = getCoords(cellID);
    console.log("i,j,x,y", i, j, x, y)
    changeBoard(i,j,x,y);
    promote(x,y);
    showGamePieces(8, 8);
    nextTurn();
    unhighlight();
    threatened([x,y],turn)
}
