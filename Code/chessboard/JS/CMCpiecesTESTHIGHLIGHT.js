var pieces = [
    [0, 0, 0, 0, 0, 0, -6, 0],
    [0, 0, 0, 0, 0, -1, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, -5],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1],
    [0, 0, 0, 0, 5, 0, 6, 0]
];

function showGamePieces(x, y){
    for(i=0; i<x; i++){
        for(j=0; j<y; j++){
            var td=getCellID(8, i, j);
            if(pieces[i][j]==-1){
                document.getElementById(td).className="bPawn";
            }else if(pieces[i][j]==1){
                document.getElementById(td).className="wPawn";
            }else if(pieces[i][j]==-2){
                document.getElementById(td).className="bRook";
            }else if(pieces[i][j]==2){
                document.getElementById(td).className="wRook";
            }else if(pieces[i][j]==-3){
                document.getElementById(td).className="bKnight";
            }else if(pieces[i][j]==3){
                document.getElementById(td).className="wKnight";
            }else if(pieces[i][j]==-4){
                document.getElementById(td).className="bBishop";
            }else if(pieces[i][j]==4){
                document.getElementById(td).className="wBishop";
            }else if(pieces[i][j]==-5){
                document.getElementById(td).className="bQueen";
            }else if(pieces[i][j]==5){
                document.getElementById(td).className="wQueen";
            }else if(pieces[i][j]==-6){
                document.getElementById(td).className="bKing";
            }else if(pieces[i][j]==6){
                document.getElementById(td).className="wKing";
            }
        }
    }
}
