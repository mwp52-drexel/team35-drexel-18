function multiMove(dir,myPiece){
    var rList = [];
    var next = true;
    var vert = 0;
    var horiz = 0;
    for (var z = 0; z < dir.length; z++){
        var way = dir[z];
        console.log("Direction way = ",way); //FIXME
        while(next == true){ //up
            vert = vert + way[1];
            horiz = horiz + way[0];
            // out of board
            if(j+vert < 0 || j+vert > 7 || i+horiz < 0 || i+horiz >7){break;}
            // empty space
            else if(pieces[j+vert][i+horiz] == 0){rList.push([horiz,vert]);}
            // black piece to white piece
            else if(pieces[j+vert][i+horiz] > 0 && myPiece < 0){rList.push([horiz,vert]); break;}
            // white piece to black piece
            else if(pieces[j+vert][i+horiz] < 0 && myPiece > 0){rList.push([horiz,vert]); break;}
            // same type piece
            else{break;}
        }
        vert = 0;
        horiz = 0;
    }
    console.log("Rook list = ",rList);
    return rList;
}



function pieceMoves(myPiece){
	var rList = [];
	if (myPiece == -1) { //black pawn
			console.log("tdID = ", tdID);
			console.log("I and J =", i, j);
			if (pieces[j+1][i+0] == 0){
					rList.push([0,+1]);
					if (tdID > 7 && tdID <16 && pieces[j+2][i+0] == 0){rList.push([0,+2]);}
			}
			if (pieces[j+1][i+1] > 0){rList.push([+1,+1]);}
			if (pieces[j+1][i-1] > 0){rList.push([-1,+1]);}
			return rList;
	}

	else if (myPiece == 1) { //white pawn
			console.log("tdID = ", tdID);
			console.log("I and J =", i, j);
			if (pieces[j-1][i+0] == 0){
					rList.push([0,-1]);
					if (tdID > 47 && tdID <56 && pieces[j-2][i+0] == 0){rList.push([0,-2]);}
			}
			if (pieces[j-1][i-1] < 0){rList.push([-1,-1]);}
			if (pieces[j-1][i+1] < 0){rList.push([+1,-1]);}
			return rList;
	}

	else if (myPiece == 2 || myPiece == -2) { //rooks
			var dir = [
			    [0,-1], //up
                [0,1],  //down
                [1,0],  //right
                [-1,0], //left
            ];
			return multiMove(dir,myPiece);
	}

	else if (myPiece == 3 || myPiece == -3) { //knights
		return [
		    [-1,-2],
            [-2,-1],
            [1, -2],
            [2, -1],
            [-1,2],
            [-2,1],
            [1,2],
            [2,1]
        ];
	}

	else if (myPiece == 4 || myPiece == -4){ //bishops
            var dir = [
                [1,-1], //up,right
                [-1,-1],  //up, left
                [1,1],  //down, right
                [-1,1], //down, left
            ];
            return multiMove(dir,myPiece);
	}
	else if (myPiece == 6 || myPiece == -6){ //kings
		return [
		    [1,0],
            [-1,0],
            [0,1],
            [0,-1],
            [1,1],
            [1,-1],
            [-1,1],
            [-1,-1]
        ];
	}
	else if (myPiece == 5 || myPiece == -5){ //queens
            var dir = [
                [0,-1], //up
                [0,1],  //down
                [1,0],  //right
                [-1,0], //left
                [1,-1], //up,right
                [-1,-1],  //up, left
                [1,1],  //down, right
                [-1,1], //down, left
            ];
            return multiMove(dir,myPiece);
	}
}
