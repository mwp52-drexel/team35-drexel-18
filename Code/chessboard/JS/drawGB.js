/*
 * Returns a unique row ID for every given place on the checkerboard
 */
function getCellID(colsPerRow, r, c){
    var id = colsPerRow*r + c;
    return id;
}

/*
 * Draws checkerboard with colors
 */

function drawGameBoard(x, y){
    var color = 0; //Used to determine the color of each space
    var table = document.getElementById("board");
    document.getElementById("test").innerHTML = "White's Turn";
    for(i=0; i<x; i++){
        if(i%2===0){ //When the row number is divisible by 2, a black square is used
            color=0;
        }else{
            color=1;
        }
        var row = document.createElement("tr");
        for(j=0; j<y; j++){ //Creates cells
            var cell = document.createElement("td");
            var cellID = getCellID(8, i, j);
            cell.id=cellID; //Assigns unique IDs for every space
            //cell.onclick=function() { highlight(event.target.id); }; //SOMEHOW THIS MAKES THE ON-CLICK TURN TO HIGHLIGHT
            cell.onclick=function() { start(event.target.id); };  //Starts getCoords which will do it's thing
            if(color===0){ //Assigns colors
                cell.className="black";
                color=1; //Switches color every space
            }else{
                cell.className="white";
                color=0;
            }
            row.appendChild(cell);
        }
        table.appendChild(row);
    }
}
